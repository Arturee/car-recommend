# Technologies
R

# Introduction
Learning user preferences in a recommender system for selling cars. Learning is done by interpolating from data that the user has rated. Tests and measurements of correctness of the resulting model are also part of the solution.

# Jak spustit
Pomoci R-studio.
- main.R > odkomentovat #install.packages > ctrl + A > run
- cas: 20s
- cas, pokud se smaze config/config.csv: vice nez 1h

Fungovani
- funkce Rate.testData() (viz editable.R) natrenuje na Train.csv a ohodnoti Test.csv,
hyperparametry si precte z config/config.csv
- pokud config/config.csv neexistuje, automaticky se vytvori. Nejlepsi hyperparametry
se zjisti pomoci five-fold prozkoumavani vsech moznych konfiguraci nad Test.csv
(viz editable.R > Hyperparams.train())
- vsechny funkce dostavaji parametr CONFIG, kt. se nastavi na zacatku a podle nej pak
vyocet probiha
- funkce pro uceni se preferenci na spojite domene: discretize, linear, polyline, peak
- diskretizace pouziva stejne dlouhe intervaly
- peak metoda bere jako predel nejvyse hodnocenou hodnotu (je naimplementovane i hledani
optimalni hodnoty pro predel, ale trva to moc dlouho na velkych datech)
(viz Pref.learn.local.continuous.regression.getBestPeakIdx)
- polyline je mars regression
- regrese (linear, polyline a peak) jsou opravene aby nevracely hodnoty zaporne nebo
vetsi nez 1
- zpusoby hledani vah: RMSE, KENDALL
- RMSE: 	vaha =  max(1/RMSE, minerror) (minerror je parametr z config)
- KENDALL:	vaha = abs(kendall)
			kendall < 0 => ivnertovat lokalni preferenci (fx = 1-fx)
            	kendall is.na => cor = max( 1 - variance x - variance y,  0)
			(nastane, pokud jeden vektor ma vsechny hodnoty identicke)
- spravnou praci se vstupnimy daty zajistuje globalni promenna COLNAMES

# Testy
viz testRunner.R a tests/