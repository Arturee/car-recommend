# R studo
# tools > global options > appearance (theme: merbiovre soft, font: lucida console, size: 11)
#                       > code (tab width: )
#   
#  Alt+O   collapse all functions
#  Ctrl+I   reindent lines






# install packages and use already installed packages
install.packages("arules")
library(arules)


#to be able to read files, set the global working directory correctly:
#Tools > Global options > set default working direcotry


#check verion and other info
version
#numeric abilities and constants of my pc
?.Machine


vector_names <- c("Citroen", "Honda", "Land Rover", "Rnage Rover", "Biggie")

#data frame = list of vectors of SAME LENGTH
car.data <- data.frame(
  c(1:5),
  vector_names
)
colnames(car.data) <- c("id", "type")
typeof(car.data)


#print
print(car.data)
print("muj textik")

summary(car.data)



#dots are just an allowed char (they only have a special meaning when used with functions)
car.data.a.b.c <- "Just a name"
print(car.data.a.b.c)




#first two rows (as data.frame)
first_two_rows <- car.data[c(1:2),]    #the colon is crucial there
#selected rows
selected_rows <- car.data[c(1,4,5),]
#first column (as vector)
first_col <- car.data[,c(1)]
#column named type (as vector)
type_col <- car.data$type
# as data frame
type_col = car.data[, "type", drop=FALSE]
#access multiple named columns
named_cols <- data.frame(car.data$type, car.data$id)
#cell 2,2 (as factor, TODO what is a factor)
cell_22 <- car.data[c(2), c(2)]
#assign #TODO care doesnt work with strings because they are converted to factors from datafrma wtf?
car.data[2,1] = 666

#indices ordered by col
indices_ordered_by_cartype <- order(car.data$type)
#data frame ordered, using ordered indices
data_frame_ordered_by_cartype <- car.data[indices_ordered_by_cartype, ]

#filter indices
#indices of only Honda and Range rover
#care logical ops have to be single!
filtered_indices <- which(car.data$type == "Honda" | car.data$type == "Rnage Rover")

#filter values (get a data frame)
#nice syntax
filtered_values <- subset(car.data, type=="Honda" | type=="Rnage Rover")


#testing presence
a <- c(1,4,6)
b <- c(4,5,6)
#for each in a get a true/false value (vector)
a_in_b <- a %in% b
#true only if all from a are in b
all_a_in_b <- all(a %in% b)



#function
f <- function(y){
  #for looop
  for (x in c(1:y)){
    print(x)
  }  
}
f(4)



#order a list
mylist = mylist[order(idxVector)]

#TODO "join"
#TODO read csv .... 


#linearni model (linearni regrese)
sample.data <- data.frame(c(0,1,2,3,4,5,6,7,8,9), c(0,0,0,0,3.4,3.9,5,4.5,3,2))
colnames(sample.data) <- c("id", "w") #x, y

#linear regression has formula y~x
fit_full <- lm(sample.data$w~sample.data$id)
abline(fit_full)
print(fit_full)
#linear regression of subset
first_half <- sample.data[c(1:4),]

#empty graph (type=nothing)
plot(c(0,9), c(0,5), type = "n") 
points(sample.data$id, sample.data$w, type = "p")
#TODO lower doesnt fucking work
fit_1half <- lm(sample.data$w~sample.data$id, sample.data[1:4,])
print(fit_1half)
abline(fit_1half)







dt1 <- c(0,1,2,3,4,5,6,7,8,9)
dt2 <- c(0,1,2,3,3.4,3.9,5,4.5,3,2)
#automatically sets colnames
df <- data.frame(id=dt1, weight=dt2)
peak <- c(4)
#colnames, subset of data
lm1 <- lm(w~id, df[0:peak,])
peak2 <- peak + 3
lm2 <- lm(id~w, df[peak:peak2,])
plot(c(0,9), c(0,5), type = "n") 
abline(lm1)
abline(lm2)
points(dt1, dt2, type = "p")







peak <- which.max(df$dt2)
plot(c(0,9), c(0,5), type = "n") 
points(c(df$dt1[1], df$dt1[peak], df$dt1[nrow(df)]) , c(df$dt2[1], df$dt2[peak], df$dt2[nrow(df)]), type = "l")
points(dt1, dt2, type = "p")


# lambdas
run = function(f){
    print(f())
}
run(function(){
    return(666)
})


cars = read.table("data/Cars.csv", header = TRUE, sep=";")
data = data.frame(id=cars$id, mil=cars$mileage)

cont = cars$id
rat = cars$mileage
mod = lm(rat~cont)
mod <- lm(mil~id, data)

prediction = predict(mod, data)
comparison = data.frame(data$mil, prediction)

x = c(19000, 20000)
plot(x, c(min(data$mil),max(data$mil)), type = "n") 
abline(mod)
points(data$id, data$mil, type = "p")
cor = cor(x=data$mil, y=prediction, method="kendall", use="pairwise")
print(cor)


# set intersection
x = 1:6
y = c(2,4,12,13,14)
z = intersect(x, y)
print(z)
w = which(y %in% c(12,13,2))
print(w)







x = c(1,2,3,4,5)
y = c(5,4,3,2,1)
cor = cor(x=x, y=y, method="kendall", use="pairwise")





frame = data.frame(
    a=c(1,2,3,4),
    b=c(6,6,6,6),
    c=c(6,6,7,7)
)






install.packages("earth")
library(earth)


dt1 <- c(0,1,2,3,4,5,6,7,8,9)
dt2 <- c(0,1,2,3,3.4,3.9,5,4.5,3,2)
df <- data.frame(dt1, dt2)

#various regression methods (MARS)


mod <- earth(dt2~dt1, df)
mod2 <- lm(dt2~dt1, df)

summary(mod)
summary(mod2)


plot(c(0,9), c(0,5), type = "n") 
plotmo(mod)
points(dt1, dt2, type = "p")
lines(dt1,dt2 ,col="green")
pred = predict(mod, newdata = df)
points(dt1, pred, type = "p", col="red")


plot(c(0,9), c(0,5), type = "n") 
abline(mod2)
points(dt1, dt2, type = "p")
lines(dt1,dt2 ,col="green")

peak <- which.max(df$dt2)
lm1 <- lm(dt2~dt1, df[0:peak,])
lm2 <- lm(dt2~dt1, df[peak:nrow(df),])
plot(c(0,9), c(0,5), type = "n") 
abline(lm1)
abline(lm2)
points(dt1, dt2, type = "p")




peak <- which.max(df$dt2)
plot(c(0,9), c(0,5), type = "n") 
points(c(df$dt1[1], df$dt1[peak], df$dt1[nrow(df)]) , c(df$dt2[1], df$dt2[peak], df$dt2[nrow(df)]), type = "l")

points(dt1, dt2, type = "p")
