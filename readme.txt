# Artur Finger
# 18.4.2017
#
# 
# POPIS --------------------------------------------------------
# SPUSTENI 
# - main.R > odkomentovat #install.packages > ctrl + A > run
# - cas: 20s
# - cas, pokud se smaze config/config.csv: vice nez 1h
#
# FUNGOVANI
# - funkce Rate.testData() (viz editable.R) natrenuje na Train.csv a ohodnoti Test.csv,
#	hyperparametry si precte z config/config.csv
# - pokud config/config.csv neexistuje, automaticky se vytvori. Nejlepsi hyperparametry
#	se zjisti pomoci five-fold prozkoumavani vsech moznych konfiguraci nad Test.csv
#	(viz editable.R > Hyperparams.train())
# - vsechny funkce dostavaji parametr CONFIG, kt. se nastavi na zacatku a podle nej pak
#	vyocet probiha
# - funkce pro uceni se preferenci na spojite domene: discretize, linear, polyline, peak
#	- diskretizace pouziva stejne dlouhe intervaly
#	- peak metoda bere jako predel nejvyse hodnocenou hodnotu (je naimplementovane i hledani
#	optimalni hodnoty pro predel, ale trva to moc dlouho na velkych datech)
#	(viz Pref.learn.local.continuous.regression.getBestPeakIdx)
#	- polyline je mars regression
#	- regrese (linear, polyline a peak) jsou opravene aby nevracely hodnoty zaporne nebo
#	vetsi nez 1
# - zpusoby hledani vah: RMSE, KENDALL
#	- RMSE: 	vaha =  max(1/RMSE, minerror) (minerror je parametr z config)
#	- KENDALL:	vaha = abs(kendall)
#				kendall < 0 => ivnertovat lokalni preferenci (fx = 1-fx)
#             	kendall is.na => cor = max( 1 - variance x - variance y,  0)
#				(nastane, pokud jeden vektor ma vsechny hodnoty identicke)
# - spravnou praci se vstupnimy daty zajistuje globalni promenna COLNAMES
#
# TESTY
# - viz testRunner.R a tests/
#
# ZAVER
# - je celkem uplne jedno zda se nekde pouzije RMSE nebo MAE (lepsi je MAE protoze je citelnejsi
#	pro cloveka)
# - uceni vah pomoci kendall korelace je lepsi nez pomoci RMSE
# - uceni lokalnich preference je nejucinejsi pomoci MARS regrese, ale je sance, ze peak metoda
#	s optimalnim predelem by mohla byt lepsi (specialne vzhledem k tomu, ze takovym zpusobem se
#	udajne generovala data)
# - R je nejblbejsi jazyk, kt. jsem kdy videl, stravil jsem vic casu debugovanim nez kodenim.
#	Chybi tridy, chybi makra. Debugovat lze jen globalni promenne - to je velky problem.
#	Konverze nedavaji smysl, spatna dokumentace. Necekane mnoho funcki v R je proste a jednoduse zbugovanych.
#	R studio taky neni nic moc. Celkove mi to zabralo 3x dele nez jsem predpokladal a prevdepodobne i
#	nez by se ocekavalo. Pravdepodobne bych cely ukol dokazal naimplementovat v Jave za polovicni cas.
#











# Par bugu v R, na kt. jsem narazil: --------------------------------------------------------
#CARE R is extremely bugged and returns typeof(dataframe) is "list"
#CARE to get a value from list use list[["val"]], list[[i]], ... (not list[i], that is a complete bug)
#CARE never use <-, always = (otherwise bugs will come)
#CARE R has function scope
    #and can be totally confused by using <- and <<- ... never use them!
# CARE errors in R are very unintelligible
# RUnit throws unused argument error
# R on the other hand notices that arguments are missing only when argument gets accessed! (not when it gets passed)
# R bug: sometimes table.read creates a list of lists instead of a data.frame .... WTF!?
# R bug: is.data.frame vrati TRUE i pro list
# R calls everything an object but doesn't actually have objects and classes
# R studio - kdyz smazu funkci, tak se nezrusi, je potreba jeste dat environment > clear
# R is bugged, it's warnings should actually be errors!
# R biggest bug i've seen so far:
#   idxs = order(continuousValues)
#   idxs = idxs[2:length(idxs)-1]
#   doesnt remove first and last element, ONLY last
#   WTF?
#   idxs = idxs[3:length(idxs)-1] does remove first and last btw
# (earth) is bugged and returns a matrix instead of a vector as result of predict()
# (Runit) checkEquals je bugged - v urcitych situacich bezduvodne haze error "names of target but not of current" (=> nelze obejit)
# R studio is bugged - changes working directory arbitrarily mid-work
# discretize() is bugged - returns <NA> as one of the intervals in some instances
# R is bugged, write.csv() ignores sep=";"
# R is bugged, is.null() not working when list element is NULL ... wtf?



# NOTES --------------------------------------------------------
# train data: 19 900 rows (merged)
#
# value ranges:
# max price 50K
# max fuel con 14
# max horsepower 340
# max tank 6210
# max milage 1440K
# date 92-2013
# models 141
# manufacturers 37




# TODOs --------------------------------------------------------
# TODO more tests
# TODO USE smart peak method, but make it faster ...
# MEBBE zkusit jinou discretize()    (# TODO discretize(dt1, method="frequency", categories=4))
# MEBBE zkusit ruzne disc bins pro kazdej atribut
# MEBE what is pairwise in kendall and should i change it?
